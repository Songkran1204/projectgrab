package com.example.clongrab.Fragment

import android.os.Build
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.RatingBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.clongrab.Adapter.AdapterRating
import com.example.clongrab.Model.RatingModel
import com.example.clongrab.R
import com.example.grab.Model.FoodMenuPage
import java.util.*
import kotlin.collections.ArrayList

class RattingFragment : Fragment(),AdapterRating.RatingInterface {
    private lateinit var newArryListRating: ArrayList<RatingModel>
    private lateinit var newRecylerviewRating: RecyclerView
    lateinit var textToSpeechEngine: TextToSpeech
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view=inflater.inflate(R.layout.fragment_ratting, container, false)
        val rattingBar:RatingBar= view.findViewById(R.id.rattingBar)
        val etComment:EditText=view.findViewById(R.id.edtComment)
        val bttSubmmit:Button=view.findViewById(R.id.bttSubmtComment)
        newArryListRating= arrayListOf()

        textToSpeechEngine = TextToSpeech(context?.applicationContext,TextToSpeech.OnInitListener { status ->
            if(status !=TextToSpeech.ERROR){
                textToSpeechEngine.language= Locale.UK

            }else{
                Log.e("Status",status.toString())
            }
        })

//        bttSubmmit.setOnClickListener {
//            val text:String=etComment.text.toString()
//            if (text.isNotEmpty()){
//                if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.LOLLIPOP){
//                    textToSpeechEngine.speak(text,TextToSpeech.QUEUE_FLUSH,null)
//                }
//            }
//        }
        bttSubmmit.setOnClickListener {
            var numStar=rattingBar.rating
            var comment=etComment.text.toString()
            val new=RatingModel(numStar,comment)
            newArryListRating.add(new)
            newRecylerviewRating=view.findViewById(R.id.recycleRating)
            newRecylerviewRating.adapter=AdapterRating(newArryListRating)

            newRecylerviewRating.layoutManager= LinearLayoutManager(context,
                LinearLayoutManager.VERTICAL,false)
            newRecylerviewRating.setHasFixedSize(true)
        }

        return view
    }

    override fun onClick(rating: RatingBar) {
        print(rating.numStars)
    }

}