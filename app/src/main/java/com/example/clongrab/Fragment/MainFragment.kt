package com.example.clongrab.Fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.clongrab.Adapter.Adapter
import com.example.clongrab.CoinActivity
import com.example.clongrab.Model.Menus
import com.example.clongrab.R
import com.example.clongrab.SpeachActivity

class MainFragment : Fragment() {
    private lateinit var newRecylerview: RecyclerView
    private lateinit var newRecylerview1: RecyclerView
    private lateinit var newArryList: ArrayList<Menus>
    private lateinit var newArryList1: ArrayList<Menus>
    lateinit var foodname:Array<String>
    lateinit var price:Array<String>
    lateinit var description:Array<String>
    lateinit var imageId:Array<Int>

    lateinit var foodnameForYou:Array<String>
    lateinit var priceForYou:Array<String>
    lateinit var imageIdForYou:Array<Int>
    lateinit var bttMenu:Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
                foodname = arrayOf(
            "กระเพรา",
            "ไข่ดาว",
            "ข้าวผัด",
            "คะน้า",
        )
        imageId= arrayOf(
            R.drawable.hamburger,
            R.drawable.diet,
            R.drawable.delivery,
            R.drawable.market,
        )
        price = arrayOf(
            "฿15",
            "฿16",
            "฿88",
            "฿20",
        )

        foodnameForYou= arrayOf(
            "AAA",
            "BBB",
            "CCC",
            "DDD",
        )
        imageIdForYou= arrayOf(
            R.drawable.hamburger,
            R.drawable.diet,
            R.drawable.banner,
            R.drawable.card,
        )
        priceForYou= arrayOf(
            "฿15",
            "฿16",
            "฿88",
            "฿20",
        )
        description = arrayOf(
            "AAAAAAAA",
            "BBBBBBBBB",
            "CCCCCCCC",
            "DDDDDDD",
        )
        val view=inflater.inflate(R.layout.fragment_main, container, false)

        bttMenu=view.findViewById(R.id.bttFood)
        newRecylerview=view.findViewById(R.id.recyclerMenu)
        newRecylerview1=view.findViewById(R.id.recyclerMenuForYou)
        newArryList= arrayListOf<Menus>()
        newArryList1= arrayListOf<Menus>()
        MenuRecyclerView(newRecylerview)
        MenuRecyclerView(newRecylerview1)
        getUserData()
        bttMenu.setOnClickListener {
            val intent= Intent(context, SpeachActivity::class.java)
            startActivity(intent)
        }

        return view
    }
        private fun MenuRecyclerView(recyclerView: RecyclerView){
        recyclerView.layoutManager= LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false)
        recyclerView.setHasFixedSize(true)

    }

    private fun getUserData(){
        for (i in foodname.indices){
            val news= Menus(imageId[i], foodname[i], price[i], description[i])
            newArryList.add(news)

        }

        for (i in foodnameForYou.indices){
            val news= Menus(imageIdForYou[i],foodnameForYou[i], priceForYou[i],description[i])
            newArryList1.add(news)

        }
        newRecylerview.adapter= Adapter(newArryList)
        newRecylerview1.adapter= Adapter(newArryList1)
    }
}