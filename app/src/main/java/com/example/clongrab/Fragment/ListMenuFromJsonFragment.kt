package com.example.clongrab.Fragment

import android.os.Bundle
import android.system.Os.open
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.clongrab.R
import com.example.grab.Adapter.AdapterListMenu
import com.example.grab.Model.FoodMenuPage
import org.json.JSONObject
import java.io.File
import java.io.InputStream

class ListMenuFromJsonFragment : Fragment() {
    private lateinit var newArryListFoodMenuPage2: ArrayList<FoodMenuPage>
    private lateinit var newRecylerviewPage2: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view=inflater.inflate(R.layout.fragment_list_menu_from_json, container, false)
        readJsonFile(view)
        return view
    }
    fun readJsonFile(view: View): String? {
        var json:String?=null
        newArryListFoodMenuPage2 = arrayListOf<FoodMenuPage>()
        try {
            val inputStram:InputStream=resources.openRawResource(R.raw.json_data)
            json=inputStram.bufferedReader().use { it.readText() }
            var jsonObject= JSONObject(json)
            val jsonArray = jsonObject.optJSONArray("data")
            for (i in 0 until jsonArray.length()){
                val jsonObject = jsonArray.getJSONObject(i)
                val price = jsonObject.optString("price").toInt()
                val name = jsonObject.optString("name")
                val image = jsonObject.optString("image")
                val description = jsonObject.optString("description")

                newRecylerviewPage2=view.findViewById(R.id.recyclerMenuPage2)
                val news= FoodMenuPage(image, name, price, description)
                newArryListFoodMenuPage2.add(news)

            }
//            Log.e("JsonData description", newArryListPage2.toString())
            newRecylerviewPage2.adapter= AdapterListMenu(newArryListFoodMenuPage2)
            newRecylerviewPage2.layoutManager= LinearLayoutManager(context,
                LinearLayoutManager.VERTICAL,false)
            newRecylerviewPage2.setHasFixedSize(true)

        }catch (ex:Exception){
//            ex.printStackTrace()
            Log.e("JsonData", ex.toString())
            return null
        }
        return json
    }
}