package com.example.clongrab.Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.clongrab.Adapter.AdapterShowListFood
import com.example.clongrab.DBHelpper.DBHelper
import com.example.clongrab.R
import com.example.grab.Model.FoodModel

class DataSQLiteFragment : Fragment() {
    lateinit var food_name: EditText
    lateinit var food_price: EditText
    lateinit var food_description: EditText
    lateinit var bttSubmit: Button
    lateinit var dbHelper: DBHelper
    lateinit var name:String
    lateinit private var newRecylerviewShowFood: RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view=inflater.inflate(R.layout.fragment_data_s_q_lite, container, false)
        initView(view)
        dbHelper= DBHelper(context)
        getFood(view)

        var data:ArrayList<FoodModel>
        bttSubmit.setOnClickListener {
            AddFood()
            getFood(view)
        }

        return view
    }
    private fun initView(view: View) {
        food_name=view.findViewById(R.id.etFoodName)
        food_price=view.findViewById(R.id.etPrice)
        food_description=view.findViewById(R.id.etDescription)
        bttSubmit=view.findViewById(R.id.bttSubmitAddFood)

    }

    private fun getFood(view: View){
        newRecylerviewShowFood=view.findViewById(R.id.recycleFood)
        val foodList=dbHelper.getAllData()
        newRecylerviewShowFood.adapter= AdapterShowListFood(foodList)
        newRecylerviewShowFood.layoutManager=
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL,false)

    }
    private fun AddFood(){
        val name=food_name.text.toString()
        val price=food_price.text.toString()
        val description=food_description.text.toString()

        if (name.isEmpty()||price.isEmpty()||description.isEmpty()){
            Toast.makeText(context,"Enter field", Toast.LENGTH_SHORT).show()
        }else{
            val food= FoodModel(0,name,price.toInt(),description)
            val status=dbHelper.insertFood(food)

            if(status>-1){
                Toast.makeText(context,"Food Add", Toast.LENGTH_SHORT).show()
                clearEditText()
            }else{
                Toast.makeText(context,"Not SAve", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun clearEditText() {
        food_price.setText("")
        food_name.setText("")
        food_description.setText("")
    }

}