package com.example.clongrab.Fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.clongrab.CoinActivity
import com.example.clongrab.R
import com.example.grab.Adapter.AdapterCoin
import com.example.grab.Api.ApiCoinService
import com.example.grab.Model.CoinModel
import com.example.grab.Model.CoinsItem
import retrofit2.Response

class CoinFragment : Fragment() {
    private lateinit var newRecylerviewCoin: RecyclerView
    private lateinit var newArryListCoin: ArrayList<CoinsItem>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view=inflater.inflate(R.layout.fragment_coin, container, false)
        val apiService: ApiCoinService = ApiCoinService()
        newArryListCoin= arrayListOf()
        val call=apiService.getData()
        call.enqueue(object :retrofit2.Callback<CoinModel>{
            override fun onResponse(
                call: retrofit2.Call<CoinModel>,
                response: Response<CoinModel>
            ) {
                if (response.isSuccessful){
                    var data=response.body()
                    for (i in 0 until (data?.data?.coins!!.size)){
                        val new=data?.data?.coins!![i]
                        newArryListCoin.add(new!!)
                    }
                    newArryListCoin.sortBy {
                        it.id
                    }
                    newRecylerviewCoin=view.findViewById(R.id.reecycleCoin)

                    var adapter=AdapterCoin(newArryListCoin)
                    newRecylerviewCoin.adapter= adapter
                    newRecylerviewCoin.layoutManager= LinearLayoutManager(context,
                        LinearLayoutManager.VERTICAL,false)
                    newRecylerviewCoin.setHasFixedSize(true)

                    adapter.setOnItemClickListener(object :AdapterCoin.onItemClickListener{
                        override fun onItemClick(position: Int) {
                            val intent=Intent(context,CoinActivity::class.java)
                            intent.putExtra("id",newArryListCoin[position].id)
                            intent.putExtra("name",newArryListCoin[position].name)
                            intent.putExtra("symbol",newArryListCoin[position].symbol)
                            intent.putExtra("iconUrl",newArryListCoin[position].iconUrl)
                            intent.putExtra("price",newArryListCoin[position].price)
                            intent.putExtra("color",newArryListCoin[position].color)
                            intent.putExtra("websiteUrl",newArryListCoin[position].websiteUrl)

                            startActivity(intent)
                        }

                    })
                }
            }

            override fun onFailure(call: retrofit2.Call<CoinModel>, t: Throwable) {
                Log.e("Api",t.message.toString())
            }

        })
        return view
    }
}