package com.example.clongrab

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.compose.ui.graphics.Color
import coil.ImageLoader
import coil.decode.SvgDecoder
import coil.request.ImageRequest
import com.squareup.picasso.Picasso

class CoinActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_coin)

        val coinId: TextView =findViewById(R.id.idCoin)
        val coinPrice: TextView =findViewById(R.id.priceCoin)
        val coinName: TextView =findViewById(R.id.nameCoin)
        val websiteUrlCoin: TextView =findViewById(R.id.websiteUrlCoin)
        val symbolCoin: TextView =findViewById(R.id.symbolCoin)
        val coinIcon: ImageView =findViewById(R.id.imageViewCoin)
        val linear: FrameLayout =findViewById(R.id.mainLinearCoin)

        val bundle:Bundle?=intent.extras
        val id:Int=bundle!!.getInt("id")
        val name: String? =bundle!!.getString("name")
        val symbol: String? =bundle!!.getString("symbol")
        val iconUrl: String? =bundle!!.getString("iconUrl")
        val price: String? =bundle!!.getString("price")
        val color: String? =bundle!!.getString("color")
        val websiteUrl: String? =bundle!!.getString("websiteUrl")

        coinId.setText(id.toString())
        coinPrice.setText(price)
        coinName.setText(name)
        symbolCoin.setText(symbol)
        if (websiteUrl==null){
            websiteUrlCoin.setText("ไม่มีเว็บไซต์")
        }else{
            websiteUrlCoin.setText(websiteUrl)
            websiteUrlCoin.movementMethod=LinkMovementMethod.getInstance()
        }
        try {
            linear.setBackgroundColor(android.graphics.Color.parseColor(color))
        }catch (e:Exception){
            linear.setBackgroundColor(android.graphics.Color.WHITE)
        }
        coinIcon.loadSvg(iconUrl!!)
    }
    fun ImageView.loadSvg(url: String) {
        val imageLoader = ImageLoader.Builder(this.context)
            .componentRegistry { add(SvgDecoder(this@loadSvg.context)) }
            .build()

        val request = ImageRequest.Builder(this.context)
            .crossfade(true)
            .crossfade(0)
            .data(url)
            .target(this)
            .build()

        imageLoader.enqueue(request)
    }
}