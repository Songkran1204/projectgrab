package com.example.clongrab.Adapter

import android.media.Rating
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.clongrab.Model.RatingModel
import com.example.clongrab.R
import com.example.grab.Adapter.AdapterListMenu
import com.example.grab.Model.FoodMenuPage

class AdapterRating (private val newListRating:ArrayList<RatingModel>):
    RecyclerView.Adapter<AdapterRating.MyViewHolder>()  {
    class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val rating: RatingBar =itemView.findViewById(R.id.rattingBarRecycleView)
        val comment: TextView =itemView.findViewById(R.id.tvComment)
    }

    interface RatingInterface {
        fun onClick(rating:RatingBar)
    }
    private var itemListener: RatingInterface? = null

    fun setItemListener(listener: RatingInterface) {
        itemListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView= LayoutInflater.from(parent.context).inflate(R.layout.recycle_rating,parent,false)
        return AdapterRating.MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currenItem=newListRating[position]
        holder.comment.text=currenItem.comment
        holder.rating.rating=currenItem.ratingnum
//        holder.rating.setOnRatingBarChangeListener() { ratingBar, fl, b -> itemListener?.onClick(holder.rating)  }
    }

    override fun getItemCount(): Int {
        return newListRating.size
    }
}