package com.example.clongrab

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.clongrab.Fragment.*
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var mainFragment=MainFragment()
        var listMenuFromJsonFragment=ListMenuFromJsonFragment()
        var DataSQLiteFragment=DataSQLiteFragment()
        var CoinFragment=CoinFragment()
        var RattingFragment=RattingFragment()

        replaceFragment(mainFragment)
        var menuBar: BottomNavigationView =findViewById(R.id.bottom_navigation)
        val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.page_1 -> {
                    replaceFragment(mainFragment)
//                    val Intent =Intent(this,MainActivity::class.java)
//                    startActivity(Intent)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.page_2 -> {
                    replaceFragment(listMenuFromJsonFragment)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.page_3 -> {
                    replaceFragment(DataSQLiteFragment)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.page_4 -> {
                    replaceFragment(CoinFragment)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.page_5 -> {
                    replaceFragment(RattingFragment)
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }

        menuBar.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

    }
    private fun replaceFragment(fragment: Fragment){
        if (fragment!=null){
            val transaction=supportFragmentManager.beginTransaction()
            transaction.replace(R.id.fragment_container,fragment)
            transaction.commit()
        }
    }
}