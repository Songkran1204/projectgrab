package com.example.clongrab

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.util.Log
import android.widget.Button
import android.widget.EditText
import java.util.*

class SpeachActivity : AppCompatActivity() {
    lateinit var textToSpeechEngine: TextToSpeech
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_speach)

        var edtText:EditText=findViewById(R.id.edtText)
        var bttSubmit:Button=findViewById(R.id.bttspeach)
        textToSpeechEngine = TextToSpeech(applicationContext,TextToSpeech.OnInitListener { status ->
            if(status !=TextToSpeech.ERROR){
                Log.e("Status",status.toString())
                textToSpeechEngine.language= Locale("th")

            }
            else{
                Log.e("Status",status.toString())
            }
        })


        bttSubmit.setOnClickListener {
            val text:String=edtText.text.toString()
            if (text.isNotEmpty()){
                if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP){
                    textToSpeechEngine.speak(text,TextToSpeech.QUEUE_FLUSH,null)
                }
            }

        }

    }
}